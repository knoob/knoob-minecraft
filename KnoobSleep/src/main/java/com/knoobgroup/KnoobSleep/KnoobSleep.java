package com.knoobgroup.KnoobSleep;

import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.World;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerBedEnterEvent;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scheduler.BukkitScheduler;

public final class KnoobSleep extends JavaPlugin implements Listener {	
	private List<Player> sleeping;
	BukkitScheduler scheduler = null;
	
	@Override
	public void onEnable() {
		getLogger().info("Starting Knoob Sleep.");
		getServer().getPluginManager().registerEvents(this, this);
		scheduler = getServer().getScheduler();
	}
	
	@Override
	public void onDisable() {
		getLogger().info("Ending Knoob Sleep.");
	}
	
	/* Player bed enter override hook. */
	@EventHandler
	public void EnterBed(final PlayerBedEnterEvent event) {
		/* Pass off the event to a schedule that will run after 100 ticks.
		 * (as according to https://minecraft.gamepedia.com/Bed) */
		scheduler.scheduleSyncDelayedTask(this, new Runnable() {
            public void run() {
            	Player p = event.getPlayer();
    			
    			// Check if the player is still sleeping
    			if (p.isSleeping()) {
    				// Get the sleeping players world
    				World w = p.getLocation().getWorld();
    				
    				// Clear any weather events and set the time to sunrise (0)
    				w.setStorm(false);
    				w.setTime(0L);
    				
    				// Broadcast a message to all players informing them of who they have to thank for the sunshine!
    				String msg = String.format("%s[%sKnoob Sleep%s] %s%s is now sleeping. おやすみなさい！",
    					ChatColor.DARK_GRAY, ChatColor.YELLOW, ChatColor.DARK_GRAY, ChatColor.WHITE, p.getDisplayName());
    				
    				Bukkit.broadcastMessage(msg);
    			}
            }
        }, 100L);
	}
}
