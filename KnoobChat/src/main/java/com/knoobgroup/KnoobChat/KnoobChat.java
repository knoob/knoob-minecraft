package com.knoobgroup.KnoobChat;

import org.bukkit.ChatColor;
import org.bukkit.GameMode;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.plugin.java.JavaPlugin;

public final class KnoobChat extends JavaPlugin implements Listener {
	@Override
	public void onEnable() {
		getLogger().info("Starting Knoob Chat.");
		getServer().getPluginManager().registerEvents(this, this);
	}
	
	@Override
	public void onDisable() {
		getLogger().info("Ending Knoob Chat.");
	}
	
	/* Chat override hook */
	@EventHandler
	public void chatFormat(AsyncPlayerChatEvent event) {
		// Early just in case return, since event is async
		if (event != null && event.isCancelled()) {
			return;
		}
		
		Player p = event.getPlayer();
		
		// Set the formatting for the default user rank
		String rank = "Player";
		ChatColor col = ChatColor.DARK_GREEN;
		
		// Check if the player is an op, then spectator, then me (lol) and apply the rank name and color accordingly
		if (p.hasPermission("minecraft.command.op")) {
			col = ChatColor.RED;
			rank = "OP";
		}
		else if(p.getGameMode() == GameMode.SPECTATOR) {
			col = ChatColor.LIGHT_PURPLE;
			rank = "Camera";
		}
		else if (p.getDisplayName().equalsIgnoreCase("BilbyFactor")) {
			col = ChatColor.GOLD;
			rank = "Knoob b0s$";
		}
		
		// Escape the string given by the player
		String msg = event.getMessage().replaceAll("%", "%%");
		
		// Build the formatted string [<rank>] <username>: <message>
		String out = String.format("%s[%s%s%s] %s%s: %s",
			ChatColor.DARK_GRAY, col, rank, ChatColor.DARK_GRAY, ChatColor.WHITE, p.getDisplayName(), msg);
		
		// Update the event with our final formatted string
		event.setFormat(out);
	}
}
